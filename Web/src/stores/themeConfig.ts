import { defineStore } from 'pinia';

/**
 * 布局配置
 * 修复：https://gitee.com/lyt-top/vue-next-admin/issues/I567R1，感谢@lanbao123
 * 2020.05.28 by lyt 优化。开发时配置不生效问题
 * 修改配置时：
 * 1、需要每次都清理 `window.localStorage` 浏览器永久缓存
 * 2、或者点击布局配置最底部 `一键恢复默认` 按钮即可看到效果
 */
export const useThemeConfig = defineStore('themeConfig', {
	state: (): ThemeConfigState => ({
		themeConfig: {"isDrawer":false,"primary":"#248ADE","isIsDark":false,"topBar":"#FFFFFF","topBarColor":"#000000","isTopBarColorGradual":true,"menuBar":"#FFFFFF","menuBarColor":"#000000","menuBarActiveColor":"var(--el-color-primary-light-7)","isMenuBarColorGradual":false,"columnsMenuBar":"#2594E3","columnsMenuBarColor":"#F0F0F0","isColumnsMenuBarColorGradual":true,"isColumnsMenuHoverPreload":false,"isCollapse":false,"isUniqueOpened":true,"isFixedHeader":true,"isFixedHeaderChange":false,"isClassicSplitMenu":false,"isLockScreen":false,"lockScreenTime":300,"isShowLogo":true,"isShowLogoChange":false,"isBreadcrumb":true,"isTagsview":true,"isBreadcrumbIcon":true,"isTagsviewIcon":true,"isCacheTagsView":false,"isSortableTagsView":true,"isShareTagsView":false,"isFooter":true,"isGrayscale":false,"isInvert":false,"isWatermark":false,"watermarkText":"Azura.OA","tagsStyle":"tags-style-five","animation":"fade","columnsAsideStyle":"columns-round","columnsAsideLayout":"columns-vertical","layout":"columns","isRequestRoutes":true,"globalTitle":"Azura.OA","globalViceTitle":"Azura.OA","globalViceTitleMsg":"","globalI18n":"zh-cn","globalComponentSize":"default"}
	}),
	actions: {
		setThemeConfig(data: ThemeConfigState) {
			this.themeConfig = data.themeConfig;
		},
	},
});
