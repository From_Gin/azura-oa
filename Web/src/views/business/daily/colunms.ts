export const colunms = [
    { label: '序号',type:'index',align:"center",width: '55' },
    // { label: '编号', prop: 'no', align:"center" ,width: '55' },
    { label: '项目名称', prop: 'projectName',width: '150' },
    { label: '工作内容', prop: 'dailyContent',  align:"left" ,  edit: true },
    { label: '记录时间', prop: 'workStartTime', align:"center" , width: '100' },
    { label: '今日用时', prop: 'workUse', align:"center" , width: '140',  edit: true },
    { label: '工作进度', prop: 'percent', align:"center",width: '265' },
    { label: '操作', prop: 'ope', width:"100",  align:"center"  },    
]
