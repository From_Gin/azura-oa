import request from '/@/utils/request';
enum Api {
    AddProjectDaily = '/api/projectDailys/addProjectDaily',
    GetProjectDaily = '/api/projectDailys/GetProjectDaily',
	UpdateProjectDaily = '/api/projectDailys/UpdateProjectDaily',
	AbondonProjectDaily = '/api/projectDailys/AbondonProjectDaily',
	GetDailyTotal = '/api/projectDailys/GetDailyTotal',
	GetTodayProjectDaily = '/api/projectDailys/GetTodayProjectDaily',

}

// 填写日志
export const AddProjectDaily = (params?: any) =>
	request({
		url: `${Api.AddProjectDaily}`,
		method: 'POST',
        data:params
});

// 获取日志
export const GetProjectDaily = (params?: any) =>
	request({
		url: `${Api.GetProjectDaily}`,
		method: 'POST',
        data:params
});

// 更新日志
export const UpdateProjectDaily = (params?: any) =>
	request({
		url: `${Api.UpdateProjectDaily}`,
		method: 'POST',
        data:params
});
// 中止日志
export const AbondonProjectDaily = (params?: any) =>
	request({
		url: `${Api.AbondonProjectDaily}/${params}`,
		method: 'POST',
    
});
// 获取日志统计
export const GetDailyTotal = (params?: any) =>
	request({
		url: `${Api.GetDailyTotal}`,
		data:params,
		method: 'POST',
});

// 获取日志统计
export const GetTodayProjectDaily = (params?: any) =>
	request({
		url: `${Api.GetTodayProjectDaily}`,
		data:params,
		method: 'POST',
});


