import request from '/@/utils/request';
enum Api {
    addOrUpdateProject = '/api/project/addOrUpdateProject',
    getProjects = '/api/project/getProjects',
    delProject = '/api/project/delProject',
    getOwnProjects = '/api/project/getOwnProjects',
	GetProjectDetail = '/api/project/GetProjectDetail',
	GetProjectDepartment = '/api/project/GetProjectDepartment',
	OverProject = '/api/project/OverProject',
}

// 更新和修改项目相关信息
export const addOrUpdateProject = (params?: any) =>
	request({
		url: `${Api.addOrUpdateProject}`,
		data:params,
		method: 'post'
});

// 获取项目列表
export const getProjects = (params?: any) =>
	request({
		url: `${Api.getProjects}`,
		data:params,
		method: 'post'
});

// 删除项目
export const delProject = (params?: any) =>
	request({
		url: `${Api.delProject}`,
		data:params,
		method: 'get'
});

// 获取自己的项目
export const GetOwnProjects = (params?: any) =>
	request({
		url: `${Api.getOwnProjects}`,
		data:params,
		method: 'get'
});

// 获取项目详情
export const GetProjectDetail = (params?: any) =>
	request({
		url: `${Api.GetProjectDetail}`,
		data:params,
		method: 'get'
});

// 获取项目详情
export const GetProjectDepartment = (params?: any) =>
	request({
		url: `${Api.GetProjectDepartment}`,
		data:params,
		method: 'get'
});

// 获取项目详情
export const OverProject = (params?: any) =>
	request({
		url: `${Api.OverProject}`,
		data:params,
		method: 'get'
});
