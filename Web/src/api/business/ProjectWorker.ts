import request from '/@/utils/request';
enum Api {
    GetOrgDepartment = '/api/projectWorker/GetOrgDepartment',
	GetProjectDepartment = '/api/projectWorker/GetProjectDepartment',
	GetTodayTime = '/api/projectWorker/GetTodayTime'
}

// 更新和修改项目相关信息
export const GetOrgDepartment = (params?: any) =>
	request({
		url: `${Api.GetOrgDepartment}`,
		method: 'get'
});


// 获取员工
export const GetProjectDepartment = (params?: any) =>
	request({
		url: `${Api.GetProjectDepartment}`,
		method: 'post',
		data:params,
});
// 获取员工
export const GetTodayTime = () =>
	request({
		url: `${Api.GetTodayTime}`,
		method: 'GET',
});
