﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.ProjectDailys.Dto;
public class UpdateDaily
{
    public int Id { get; set; }
    public string DailyContent { get; set; }
}
