﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.ProjectDailys.Dto;
public class ProjectDailyChildTotalOutput
{
    /// <summary>
    /// 主键
    /// </summary>
    public long Id {  get; set; }

    /// <summary>
    /// 姓名
    /// </summary>
    public string Name { get; set; }

    public List<ChildDetail> dayone { get; set; }
    public List<ChildDetail> daysecond { get; set; }
    public List<ChildDetail> daythrid {  get; set; }
    public List<ChildDetail> dayforth {  get; set; }
    public List<ChildDetail> dayfith { get; set; }
    public List<ChildDetail> daysixth {  get; set; }
    public List<ChildDetail> dayseventh { get; set; }

}

public class ChildDetail
{
    public long Id;
    public string Project;
    public Boolean? Isstudy;
    public string Content;
    public string Remarks;
    public decimal? Usetime;
}
