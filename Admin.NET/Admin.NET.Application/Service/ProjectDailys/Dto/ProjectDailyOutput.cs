﻿using Admin.NET.Core;
using Org.BouncyCastle.Asn1.Mozilla;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.ProjectDailys.Dto;
public class ProjectDailyOutput 
{  
    public long Id { get; set; }

    public long? ProjectId { get; set; }

    public string ProjectName { get; set; }

    public int No { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public long? ProjectDailyId { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public decimal? WorkUse { get; set; }
    /// <summary>
    /// 日志内容
    /// </summary>
    public string? DailyContent { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public int? Percent { get; set; }


    /// <summary>
    /// 
    /// </summary>
    public DateTime? WorkStartTime { get; set; }

    public Boolean isAbondon { get; set; }

}
