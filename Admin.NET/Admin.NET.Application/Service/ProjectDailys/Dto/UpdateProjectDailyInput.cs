﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.ProjectDailys.Dto;
public class UpdateProjectDailyInput
{
    public long Id { get; set; }
    /// 
    /// </summary>
    public long? ProjectDailyId { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public decimal? WorkUse { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string Remark { get; set; }

    /// <summary>
    /// 日志内容
    /// </summary>
    public string? DailyContent { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public int Percent { get; set; } = 0;
}
