﻿using Admin.NET.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.ProjectDailys.Dto;
public class ProjectDailyPageInput : BasePageInput
{
    public DateTime Date { get; set; }
}
