﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Projects.Dto;
public class OwnProjectOutput
{
    public long Id { get; set; }
    public string Name { get; set; }
}
