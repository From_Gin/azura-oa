﻿using Admin.NET.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Projects.Dto;
public class PageProjectInput :BasePageInput
{
    public string Name {  get; set; }

    public List<DateTime> dataRange {  get; set; }
}
