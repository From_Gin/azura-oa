﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Projects.Dto;
public class PageProjectOutput
{
    public virtual long id { get; set; }
    /// 项目名称
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// 项目开始时间
    /// </summary>
    public DateTime? StartTime { get; set; }

    public string orgName { get; set; }


    /// <summary>
    /// 项目结束时间
    /// </summary>
    [SugarColumn(ColumnName = "EndTime", ColumnDescription = "项目结束时间")]
    public DateTime? EndTime { get; set; }

    /// <summary>
    /// 项目是否结束
    /// </summary>
    [SugarColumn(ColumnName = "IsFinsih", ColumnDescription = "项目是否结束")]
    public bool? IsFinsih { get; set; }

    /// <summary>
    /// 客户
    /// </summary>
    [SugarColumn(ColumnName = "Customer", ColumnDescription = "客户", Length = 255)]
    public string? Customer { get; set; }

    /// <summary>
    /// 客户
    /// </summary>
    [SugarColumn(ColumnName = "Customer", ColumnDescription = "客户", Length = 255)]
    ///项目用时
    public Decimal projectTime { get; set; } = 0;
    //前端用时
    public Decimal frontTime { get; set; } = 0;
    //后端用时
    public Decimal backendTime { get; set; } = 0;
    public Decimal productTime { get; set; } = 0;

    public Decimal totalTime { get; set; } = 0;

    public int cost { get; set; } = 0;
}
