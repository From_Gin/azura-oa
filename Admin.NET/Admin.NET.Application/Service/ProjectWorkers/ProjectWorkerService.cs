﻿using Admin.NET.Application.Entity;
using Admin.NET.Application.Service.Projects.Dto;
using Admin.NET.Application.Service.ProjectWorkers.Dto;
using Admin.NET.Core;
using Furion.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.ProjectWorkers;
public class ProjectWorkerService : IDynamicApiController, ITransient
{
    private readonly SqlSugarRepository<ProjectWorker> _ProjectWorker;
    private readonly SqlSugarRepository<ProjectDaily> _ProjectDaily;
    private readonly SqlSugarRepository<ProjectDaily_Child> _ProjectDailyChild;

    private readonly UserManager _userManager;

    private readonly SqlSugarRepository<SysOrg> _SysOrg;
    private readonly SqlSugarRepository<SysUser> _SysUser;

    public ProjectWorkerService(SqlSugarRepository<ProjectWorker> ProjectWorker,
        SqlSugarRepository<ProjectDaily> ProjectDaily, UserManager userManager,
 SqlSugarRepository<ProjectDaily_Child> ProjectDailyChild, SqlSugarRepository<SysOrg> SysOrg, SqlSugarRepository<SysUser> SysUser)
    {
        _ProjectWorker = ProjectWorker;
        _userManager = userManager;

        _SysOrg = SysOrg;
        _ProjectDailyChild = ProjectDailyChild;
        _SysUser = SysUser;
        _ProjectDaily = ProjectDaily;
    }


    [Description("绑定项目和员工")]
    [ApiDescriptionSettings(Name = "BindProjectWorker"), HttpPost]
    public async Task<Boolean> BindProjectWorker(ProjectBindRoleInput projectWorker)
    {
        ///业务逻辑先删除后新增
        List<ProjectWorker> results  =await _ProjectWorker.AsQueryable().Where(p=>p.ProjectId.Equals(projectWorker.projectId)).ToListAsync();
        await _ProjectWorker.DeleteAsync(results);
        foreach (var temp in projectWorker.rolePart)
        {
           List<ProjectWorker> res = new List<ProjectWorker>();
           foreach (var item in temp.userId)
            {       
                ProjectWorker tempProjectWorker = new ProjectWorker();
                tempProjectWorker.ProjectId = projectWorker.projectId;
                tempProjectWorker.RoleId = temp.roleId;
                tempProjectWorker.UserId = item;
                res.Add(tempProjectWorker);
            }
            await _ProjectWorker.InsertRangeAsync(res);
        }
        return true;
    }


    public async Task BindProjectWorkers(List<ProjectMemer> projectWorker,long id)
    {
        ///业务逻辑先删除后新增
        List<ProjectWorker> results = await _ProjectWorker.AsQueryable().Where(p => p.ProjectId.Equals(id)).ToListAsync();
        await _ProjectWorker.DeleteAsync(results);
        foreach (var temp in projectWorker)
        {
            List<ProjectWorker> res = new List<ProjectWorker>();
            foreach (var item in temp.userid)
            {
                ProjectWorker tempProjectWorker = new ProjectWorker();
                tempProjectWorker.ProjectId = id;
                tempProjectWorker.RoleId = temp.id;
                tempProjectWorker.UserId = item;
                res.Add(tempProjectWorker);
            }
            await _ProjectWorker.InsertRangeAsync(res);
        }
    }


    [Description("获取所有的部门")]
    [ApiDescriptionSettings(Name = "GetOrgDepartment"), HttpGet]
    public async Task<List<SysOrg>> GetOrgDepartment()
    {
        List<SysOrg> Output = _SysOrg.AsQueryable().Where(p=>p.Type == "301").ToList();
        
        return Output;
    }


    [Description("获取项目所涉及的人")]
    [ApiDescriptionSettings(Name = "GetTodayTime"), HttpGet]
    public async Task<Decimal> GetTodayTime()
    {
        Decimal Output = 0;
        DateTime today = DateTime.Today;
        DateTime nextday = DateTime.Today.AddDays(1);
        List<ProjectDaily_Child> projectDailylist = await _ProjectDailyChild.AsQueryable().Where(p => p.CreateUserId.Equals(_userManager.UserId) && p.ToWrittenTime >= today && p.ToWrittenTime < nextday).ToListAsync();
        foreach (ProjectDaily_Child item in projectDailylist) {
            Boolean isExist = await _ProjectDaily.AsQueryable().AnyAsync(p => p.isAbondon.Equals(true) && p.Id.Equals(item.ProjectDailyId));
            if(isExist)
            {
                continue;
            }
            Output = (decimal)(Output + item.WorkUse);
        }
        return Output;
    }



    [DisplayName("显示部门下所有的人并且分组")]
    [ApiDescriptionSettings(Name = "GetProjectDepartment"), HttpPost]
    public async Task<List<OrgWorkersGroupOutput>> GetProjectDepartment(GetProjectDepartmentInput Input)
    {
        List<OrgWorkersGroupOutput> Output = new List<OrgWorkersGroupOutput>();
        //foreach(var company in Input.companys)
        //{
        if (Input.companys.IsNullOrEmpty())
        {
            return Output;
        }
        SysOrg sysOrg = await _SysOrg.AsQueryable().FirstAsync(p => p.Id.Equals(Input.companys));
        //    //先获取第一组的数据
        List<SysOrg> SysOrgList = await _SysOrg.AsQueryable().Where(p => p.Pid.Equals(Input.companys)).ToListAsync();
            foreach(var temp in SysOrgList) {
                OrgWorkersGroupOutput baseOrgGroup = Output.Find(p => p.label.Equals(temp.Name));
                if(baseOrgGroup == null) {
                    baseOrgGroup = new OrgWorkersGroupOutput();
                    baseOrgGroup.label = temp.Name;
                    baseOrgGroup.options = new List<OrgWorkersGroup>();
                    List<SysUser> sysUsers = await _SysUser.AsQueryable().Where(p => p.OrgId.Equals(temp.Id))
                   .WhereIF(!Input.name.IsNullOrEmpty(), p => p.NickName.Contains(Input.name) || p.RealName.Contains(Input.name))
                   .ToListAsync();
                    foreach (var user in sysUsers)
                    {
                    if (sysOrg.IsNullOrEmpty())
                    {
                        baseOrgGroup.options.Add(new OrgWorkersGroup
                        {
                            value = user.Id,
                            label = user.NickName + "(" + user.RealName + ")" ,
                        });
                    }
                    else
                    {
                        baseOrgGroup.options.Add(new OrgWorkersGroup
                        {
                            value = user.Id,
                            label = user.NickName + "(" + user.RealName + ")," + sysOrg.Name,
                        });
                    }
                    }
                    Output.Add(baseOrgGroup);
                }
                else
                {
                    List<SysUser> sysUsers = await _SysUser.AsQueryable().Where(p => p.OrgId.Equals(temp.Id))
                   .WhereIF(Input.name.IsNullOrEmpty(), p => p.NickName.Contains(Input.name) || p.RealName.Contains(Input.name))
                   .ToListAsync();
                    foreach (var user in sysUsers)
                    {
                        baseOrgGroup.options.Add(new OrgWorkersGroup
                        {
                            value = user.Id,
                            label = user.NickName + "(" + user.RealName + ")," + sysOrg.Name,
                        });
                }
            }
        }
        //}
        return Output;
    }

}
