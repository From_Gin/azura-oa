﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.ProjectWorkers.Dto;
public class OrgWorkersGroupOutput
{
    public string label { get; set; }
    public List<OrgWorkersGroup> options { get; set; }
}

public class OrgWorkersGroup
{
    public string label { get; set; }
     public long value { get; set; }
}