﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.ProjectWorkers.Dto;
public class ProjectBindRoleInput
{
    public long projectId { get; set; }
    public List<rolePart> rolePart { get; set; }
}
public class rolePart
{
    public List<long> userId { get; set; }
    public long roleId { get; set; }
}
