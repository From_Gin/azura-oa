﻿using Admin.NET.Core;
namespace Admin.NET.Application.Entity;

/// <summary>
/// 
/// </summary>
[SugarTable("ProjectDaily","")]
public class ProjectDaily  : EntityBase
{
    /// <summary>
    /// 
    /// </summary>
    [SugarColumn(ColumnName = "ProjectId", ColumnDescription = "")]
    public long? ProjectId { get; set; }
    
    /// <summary>
    /// 
    /// </summary>
    [SugarColumn(ColumnName = "DailyContent", ColumnDescription = "", Length = 255)]
    public string? DailyContent { get; set; }
    
    /// <summary>
    /// 
    /// </summary>
    [SugarColumn(ColumnName = "UseTime", ColumnDescription = "", Length = 18, DecimalDigits=2 )]
    public decimal? UseTime { get; set; }
    
    /// <summary>
    /// 
    /// </summary>
    [SugarColumn(ColumnName = "Percent", ColumnDescription = "")]
    public int Percent { get; set; }
    
    /// <summary>
    /// 
    /// </summary>
    [SugarColumn(ColumnName = "IsStudy", ColumnDescription = "")]
    public bool? IsStudy { get; set; }
    
    /// <summary>
    /// 
    /// </summary>
    [SugarColumn(ColumnName = "No", ColumnDescription = "")]
    public int? No { get; set; }


    /// <summary>
    /// 任务登记时间
    /// </summary>
    [SugarColumn(ColumnName = "WorkStartTime", ColumnDescription = "")]
    public DateTime? WorkStartTime { get; set; }


    /// <summary>
    /// 任务结束时间
    /// </summary>
    [SugarColumn(ColumnName = "WorkEndTime", ColumnDescription = "")]
    public DateTime? WorkEndTime { get; set; }
    /// <summary>
    /// 是否被中止
    /// </summary>

    [SugarColumn(ColumnName = "isAbondon", ColumnDescription = "")]
    public Boolean isAbondon { get; set; }
}
