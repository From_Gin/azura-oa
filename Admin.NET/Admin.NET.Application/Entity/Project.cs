﻿using Admin.NET.Core;
namespace Admin.NET.Application.Entity;

/// <summary>
/// 项目表
/// </summary>
[SugarTable("Project","项目表")]
public class Project  : EntityBase
{
    /// <summary>
    /// 项目名称
    /// </summary>
    [SugarColumn(ColumnName = "Name", ColumnDescription = "项目名称", Length = 255)]
    public string? Name { get; set; }
    
    /// <summary>
    /// 项目开始时间
    /// </summary>
    [SugarColumn(ColumnName = "StartTime", ColumnDescription = "项目开始时间")]
    public DateTime? StartTime { get; set; }
    
    /// <summary>
    /// 项目结束时间
    /// </summary>
    [SugarColumn(ColumnName = "EndTime", ColumnDescription = "项目结束时间")]
    public DateTime? EndTime { get; set; }
    
    /// <summary>
    /// 项目是否结束
    /// </summary>
    [SugarColumn(ColumnName = "IsFinsih", ColumnDescription = "项目是否结束")]
    public bool? IsFinsih { get; set; }
    
    /// <summary>
    /// 客户
    /// </summary>
    [SugarColumn(ColumnName = "Customer", ColumnDescription = "客户", Length = 255)]
    public string? Customer { get; set; }
    
}
