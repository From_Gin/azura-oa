﻿using Admin.NET.Core;
namespace Admin.NET.Application.Entity;

/// <summary>
/// 
/// </summary>
[SugarTable("ProjectDaily_Child","")]
public class ProjectDaily_Child  : EntityBase
{
    /// <summary>
    /// 
    /// </summary>
    [SugarColumn(ColumnName = "ProjectDailyId", ColumnDescription = "")]
    public virtual long? ProjectDailyId { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [SugarColumn(ColumnName = "WorkUse", ColumnDescription = "", Length = 18, DecimalDigits = 2)]
    public decimal? WorkUse { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    [SugarColumn(ColumnName = "Remark", ColumnDescription = "", Length = 255)]
    public string Remark { get; set; }

    /// <summary>
    /// 日志内容
    /// </summary>
    [SugarColumn(ColumnName = "DailyContent", ColumnDescription = "", Length = 255)]
    public string DailyContent { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [SugarColumn(ColumnName = "Percent", ColumnDescription = "")]
    public int? Percent { get; set; } = 0;
    
    [SugarColumn(ColumnName = "ToWrittenTime", ColumnDescription = "")]
    public DateTime? ToWrittenTime { get; set; }
}
