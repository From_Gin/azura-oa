﻿using Admin.NET.Core;
namespace Admin.NET.Application.Entity;

/// <summary>
/// 项目和人员的中间表
/// </summary>
[SugarTable("ProjectWorker","项目和人员的中间表")]
public class ProjectWorker  : EntityBase
{
    /// <summary>
    /// 
    /// </summary>
    [SugarColumn(ColumnName = "ProjectId", ColumnDescription = "")]
    public long ProjectId { get; set; }
    
    /// <summary>
    /// 
    /// </summary>
    [SugarColumn(ColumnName = "UserId", ColumnDescription = "")]
    public long UserId { get; set; }
    
    /// <summary>
    /// 
    /// </summary>
    [SugarColumn(ColumnName = "RoleId", ColumnDescription = "")]
    public long RoleId { get; set; }
    
}
