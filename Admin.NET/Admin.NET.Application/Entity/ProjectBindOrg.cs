﻿using Admin.NET.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Entity;
/// <summary>
/// 项目相关的组织
/// </summary>
[SugarTable("ProjectBindOrg", "项目关联的部门表")]
public class ProjectBindOrg: EntityBaseData
{
    [SugarColumn(ColumnName = "ProjectId", ColumnDescription = "")]

    public long ProjectId { get; set; }
    [SugarColumn(ColumnName = "OrgId", ColumnDescription = "")]

    public long OrgId { get; set; }
}
